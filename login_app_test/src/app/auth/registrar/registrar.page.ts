import { Component, OnInit } from '@angular/core';
import { UserModel } from 'src/app/models/User.model';
import { RespuestaModel } from 'src/app/models/Respuesta.model';
import { AlertController } from '@ionic/angular';
import { AuthService } from '../../services/auth/auth.service';
import { Router } from '@angular/router';
import { UtilService } from '../../services/util/util.service';

@Component({
  selector: 'app-registrar',
  templateUrl: './registrar.page.html',
  styleUrls: ['./registrar.page.scss'],
})
export class RegistrarPage implements OnInit {
  user: UserModel;
  confirmPassword: string = '';
  isInvalidEmail: boolean = false;
  showPassword: boolean = false;
  showPasswordconfir: boolean = false;
  validateGerneal:any[]=[];

  constructor(
    private alertController: AlertController,
    private authSrv: AuthService,
    public router: Router,
    public util: UtilService,    

  ) {
    this.user = new UserModel();
  }

  ngOnInit() {}

  isPasswordValid() {}

  register() {
    let validate:any[]= this.validateInput(this.user.email,this.user.full_name,this.user.password,this.user.username,this.confirmPassword);
    if(validate.length==0){ 
      if(this.user.email){
        let validateEmailParnet:any[]=this.validateEmail(this.user.email);
        if(validateEmailParnet.length==0){
          this.showAlert([]);
        }else{
          this.showAlert(validateEmailParnet);
        }
      }     
      if(this.user.password){
        let validatePass:any[]=[];
        validatePass=this.validarPassword(this.user.password!);
        if(validatePass.length==0){
          if(this.user.password!=this.confirmPassword){
            this.validateGerneal.push("Contraseña no coincide");
          }
         }else{
           this.showAlert(validatePass);
  
         }
      }
          
      if(this.validateGerneal.length==0){
        this.util.showLoading().then((_) => {
          this.authSrv.registrar(this.user).subscribe((r:RespuestaModel)=>{
          this.util.dismissLoading();

            if(r.success){
              this.util.presentAlert('Modificación', '',r.message!);
              localStorage.setItem("me",JSON.stringify(r.data));
              this.router.navigate(['home']);
            }else{
              this.util.presentAlert('Error', '',r.message!);
            }
          })
        });
    
      }

    }else{
      this.showAlert(validate);
    }

  }

  validateEmail(email:string) {
    const camposFaltantes: string[] = [];
    const emailRegex = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;

    if (!emailRegex.test(email)) {
      camposFaltantes.push('El correo electrónico no es válido');
      return camposFaltantes;
    }else{
      return camposFaltantes;

    }

  }

  validateInput(email?: string, full_name?: string,password?: string,username?:string,passwordconfir?:string): string[] {
    const camposFaltantes: string[] = [];
  
    if (!email) {
      camposFaltantes.push('-Debes ingresar el email');
    }
    if (!username) {
      camposFaltantes.push('-Debes ingresar el username');
    }
    if (!full_name) {
      camposFaltantes.push('-Debes ingresar el nombre y apellido');
    }
    if (!passwordconfir) {
      camposFaltantes.push('-Debes ingresar el confrimar password');
    }
    if (!password) {
      camposFaltantes.push('-Debes ingresar el password');
    }
  
    return camposFaltantes;
  }

  togglePasswordVisibility() {
    this.showPassword = !this.showPassword;
  }

  togglePasswordConfirVisibility() {
    this.showPasswordconfir = !this.showPasswordconfir;
  }

  validarPassword(password: string): string[] {
    const simbolos = [" ", "!", "\"", "#", "$", "%", "&", "'", "(", ")", "*", "+", ",", "-", ".", "/", ":", ";", "<", "=", ">", "?", "@", "[", "\\", "]", "^", "_", "`", "{", "|", "}", "~"];
    const requisitosFaltantes: string[] = [];
  
    if (password.length < 8) {
      requisitosFaltantes.push("la contraseña debe tener al menos 8 caracteres");
    }
  
    if (!/[A-Z]/.test(password)) {
      requisitosFaltantes.push("la contraseña debe tener al menos una letra mayúscula");
    }
  
    if (!/\d/.test(password)) {
      requisitosFaltantes.push("la contraseña debe tener al menos un número");
    }
  
    let tieneSimbolo = false;
    for (const simbolo of simbolos) {
      if (password.includes(simbolo)) {
        tieneSimbolo = true;
        break;
      }
    }
  
    if (!tieneSimbolo) {
      requisitosFaltantes.push("la contraseña debe tener al menos un símbolo");
    }
  
    return requisitosFaltantes;
  }

  async showAlert(messages: string[]) {
    this.validateGerneal=[];
    this.validateGerneal=messages;
    
  }

  goTo(){
      this.router.navigate(['login']);
  }


}
