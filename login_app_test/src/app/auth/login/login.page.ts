import { Component, OnInit } from '@angular/core';
import { UserModel } from 'src/app/models/User.model';
import { RespuestaModel } from 'src/app/models/Respuesta.model';
import { AlertController } from '@ionic/angular';
import { AuthService } from '../../services/auth/auth.service';
import { Router } from '@angular/router';
import { UtilService } from '../../services/util/util.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  user:UserModel;
  emailOrUsername:string="";
  showPassword: boolean = false;
  validateGerneal:any[]=[];
  constructor(private alertController: AlertController,private authSrv: AuthService,
    public router: Router,
    public util: UtilService,    
    ) { 
    this.user= new UserModel();
  }

  ngOnInit() {
  }


  login(){

    let validate:any[]= this.validateInput(this.emailOrUsername,this.user.password);
    let validatePass:any[]=[];
    if(validate.length==0){
      if (this.alidateInput(this.emailOrUsername)=="email") {
        this.user.email=this.emailOrUsername;
        
      }else if(this.alidateInput(this.emailOrUsername)=="username"){
        this.user.username=this.emailOrUsername;
      }
       validatePass=this.validarPassword(this.user.password!);
       if(validatePass.length==0){
        this.validateGerneal=[];
       }else{
         this.showAlert(validatePass);

       }
    }else{
      this.showAlert(validate);
    }


    if(validate.length==0 && validatePass.length==0){
      this.util.showLoading().then((_) => {
        
        
        this.authSrv.login(this.user).subscribe((r:RespuestaModel)=>{
          this.util.dismissLoading();
          if(r.success){
          localStorage.setItem("me",JSON.stringify(r.data));
          this.router.navigate(['home']);
        }else{
          
        }
      })
    });
    }

  }

  alidateInput(input: string): string {
    const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  
    if (emailPattern.test(input)) {
      return 'email';
    } else {
      return 'username';
    }
  }

  validarPassword(password: string): string[] {
    const simbolos = [" ", "!", "\"", "#", "$", "%", "&", "'", "(", ")", "*", "+", ",", "-", ".", "/", ":", ";", "<", "=", ">", "?", "@", "[", "\\", "]", "^", "_", "`", "{", "|", "}", "~"];
    const requisitosFaltantes: string[] = [];
  
    if (password.length < 8) {
      requisitosFaltantes.push("la contraseña debe tener al menos 8 caracteres");
    }
  
    if (!/[A-Z]/.test(password)) {
      requisitosFaltantes.push("la contraseña debe tener al menos una letra mayúscula");
    }
  
    if (!/\d/.test(password)) {
      requisitosFaltantes.push("la contraseña debe tener al menos un número");
    }
  
    let tieneSimbolo = false;
    for (const simbolo of simbolos) {
      if (password.includes(simbolo)) {
        tieneSimbolo = true;
        break;
      }
    }
  
    if (!tieneSimbolo) {
      requisitosFaltantes.push("la contraseña debe tener al menos un símbolo");
    }
  
    return requisitosFaltantes;
  }

  togglePasswordVisibility() {
    this.showPassword = !this.showPassword;
  }

  validateInput(emailOrUsername: string, password?: string): string[] {
    const camposFaltantes: string[] = [];
  
    if (!emailOrUsername) {
      camposFaltantes.push('-Debes ingresar el email o el username');
    }
  
    if (!password) {
      camposFaltantes.push('-Debes ingresar el password');
    }
  
    return camposFaltantes;
  }

  async showAlert(messages: string[]) {
    this.validateGerneal=[];
    this.validateGerneal=messages;
    
  }


  async showAlertError(messages: string[]) {
    const alert = await this.alertController.create({
      header: 'Error de validación',
      message: messages.join('<br>'),
      buttons: ['Aceptar']
    });

    await alert.present();
  }
  
  async showSuccessAlert(message: string) {
    const alert = await this.alertController.create({
      header: 'Inicio de sesión exitoso',
      message: message,
      buttons: ['Aceptar']
    });

    await alert.present();
  }

  goTo(){
    this.router.navigate(['registrar']);

  }
}
