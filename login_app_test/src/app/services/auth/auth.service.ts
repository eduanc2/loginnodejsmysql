import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as global from '../../../global';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user:any="";
  authToken:string="";

  constructor(private http:HttpClient) {
    
   }

   header(){
    let userString = localStorage.getItem("me");
    let user:any = userString !== null ? JSON.parse(userString): null;
    let headers = new HttpHeaders()
    .set('Authorization', user.token)
    .set('Content-Type', 'application/json')
    .set('Accept', 'application/json');

    return {headers};
  }

  public login(data:any){
    return this.http.post(global.urlBase+'auth/login', data);
  }
  public registrar(data:any){
    return this.http.post(global.urlBase+'auth/registrar', data);
  }

  public update(data:any){
    return this.http.post(global.urlBase+'auth/update', data,this.header());
  }

}
