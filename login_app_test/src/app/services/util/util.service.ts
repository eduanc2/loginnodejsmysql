import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import {
  AlertController,
  LoadingController,
  ToastController,
} from '@ionic/angular';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UtilService {
  loading: any;

  constructor(
    private toastController: ToastController,
    private loadingCtrl: LoadingController,
    public router: Router,
    private alertController: AlertController
  ) {
  }


  async presentToast(
    msg: string = '',
    duration: number = 1000,
    position: 'top' | 'middle' | 'bottom' = 'bottom'
  ) {
    const toast = await this.toastController.create({
      message: msg,
      duration: duration,
      position: position,
    });

    await toast.present();
  }

  async showLoading() {
    this.loading = await this.loadingCtrl.create({
      message: '',
      duration: 1000,
    });
    return await this.loading.present();
  }

  dismissLoading() {
    this.loading.dismiss();
  }

  async presentAlert(header: string, subHeader: string, message: string) {
    const alert = await this.alertController.create({
      header: header,
      subHeader: subHeader,
      message: message,
      buttons: ['ACEPTAR'],
    });

    return await alert.present();
  }

  addUpdate(item: any, root: string, measure?: string) {
    if (measure) {
      item = {
        ...item,
        measure: measure,
      };
    }
    if (item.id != 0) {
      this.router.navigate([root + '/add-update'], {
        queryParams: item,
      });
    } else {
      this.router.navigate([root + '/add-update'], {
        queryParams: item,
      });
    }
  }
}
