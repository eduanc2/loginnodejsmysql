export class RespuestaModel {
    code?: number=200;
    message?: string="";
    success?: boolean=true;
    data?: any=null;
}