export class UserModel{
    id?:number;
    email?:string;
    username?:string;
    full_name?:string;
    type_user?:string;
    password?:string;
    status?:number;
}