"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.port = exports.config_db = void 0;
const dotenv_1 = require("dotenv");
(0, dotenv_1.config)();
exports.config_db = {
    HOST: "localhost",
    USER: "root",
    PASSWORD: "",
    DB: "test_logindb"
};
exports.port = process.env.PORT || 3000;
