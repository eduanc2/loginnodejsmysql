"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.connectToDB = void 0;
const mysql2_1 = __importDefault(require("mysql2"));
const config_1 = require("./config");
function connectToDB() {
    const connection = mysql2_1.default.createConnection({
        host: config_1.config_db.HOST,
        user: config_1.config_db.USER,
        password: config_1.config_db.PASSWORD,
        database: config_1.config_db.DB,
    });
    return new Promise((resolve, reject) => {
        connection.connect((error) => {
            if (error) {
                reject(error);
            }
            else {
                console.log("Connected to MySQL");
                resolve(connection);
            }
        });
    });
}
exports.connectToDB = connectToDB;
