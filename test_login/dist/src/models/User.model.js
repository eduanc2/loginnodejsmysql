"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.updateUser = exports.addUser = exports.getUserByUsername = exports.getUserByEmail = exports.getUser = exports.getUsers = exports.User = void 0;
const database_1 = require("../../database");
class User {
}
exports.User = User;
function getUsers() {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const connection = yield (0, database_1.connectToDB)();
            return new Promise((resolve, reject) => {
                connection.query("SELECT * FROM users", (error, results) => {
                    if (error) {
                        reject(error);
                    }
                    else {
                        resolve(results);
                    }
                });
            });
        }
        catch (error) {
            throw error;
        }
    });
}
exports.getUsers = getUsers;
function getUser(data) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const connection = yield (0, database_1.connectToDB)();
            return new Promise((resolve, reject) => {
                connection.query("SELECT * FROM users WHERE id=" + data.id, (error, results) => {
                    if (error) {
                        reject(error);
                    }
                    else {
                        resolve(results[0]);
                    }
                });
            });
        }
        catch (error) {
            throw error;
        }
    });
}
exports.getUser = getUser;
function getUserByEmail(data) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const connection = yield (0, database_1.connectToDB)();
            return new Promise((resolve, reject) => {
                connection.query("SELECT * FROM users WHERE email='" + data.email + "'", (error, results) => {
                    if (error) {
                        reject(error);
                    }
                    else {
                        resolve(results[0]);
                    }
                });
            });
        }
        catch (error) {
            throw error;
        }
    });
}
exports.getUserByEmail = getUserByEmail;
function getUserByUsername(data) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const connection = yield (0, database_1.connectToDB)();
            return new Promise((resolve, reject) => {
                let sql = "SELECT * FROM users WHERE username='" + data.username + "' or email='" + data.email + "'";
                connection.query(sql, (error, results) => {
                    if (error) {
                        reject(error);
                    }
                    else {
                        resolve(results[0]);
                    }
                });
            });
        }
        catch (error) {
            throw error;
        }
    });
}
exports.getUserByUsername = getUserByUsername;
function addUser(data) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const connection = yield (0, database_1.connectToDB)();
            return new Promise((resolve, reject) => {
                connection.query(`INSERT INTO users(email,username,full_name,password,type_user,status) VALUES('${data.email}','${data.username}','${data.full_name}','${data.password}','${data.type_user}',1)`, (error, results) => {
                    if (error) {
                        reject(error);
                    }
                    else {
                        resolve(results);
                    }
                });
            });
        }
        catch (error) {
            throw error;
        }
    });
}
exports.addUser = addUser;
function updateUser(data) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const connection = yield (0, database_1.connectToDB)();
            return new Promise((resolve, reject) => {
                connection.query(`UPDATE users SET email = '${data.email}', username = '${data.username}', full_name = '${data.full_name}', password = '${data.password}' WHERE id=${data.id}`, (error, results) => {
                    if (error) {
                        reject(error);
                    }
                    else {
                        resolve(results);
                    }
                });
            });
        }
        catch (error) {
            throw error;
        }
    });
}
exports.updateUser = updateUser;
