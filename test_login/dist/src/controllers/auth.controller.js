"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Respuesta_model_1 = require("../models/Respuesta.model");
const User_model_1 = require("../models/User.model");
const bcrypt_1 = __importDefault(require("bcrypt"));
const generar_jwt_1 = __importDefault(require("../helpers/generar-jwt"));
const authController = {};
authController.login = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const jsonRes = new Respuesta_model_1.RespuestaModel();
    let data = req.body;
    let sendUser = {};
    let user;
    let message = "";
    if (data.email) {
        user = yield (0, User_model_1.getUserByEmail)({
            email: data.email,
        });
        if (user) {
            message = "usuario con email " + data.email + " no encontrado";
        }
    }
    else if (data.username) {
        user = yield (0, User_model_1.getUserByUsername)({
            username: data.username,
        });
        if (!user) {
            message = "usuario con username " + data.username + "  no encontrado";
        }
    }
    if (user) {
        const hash = bcrypt_1.default.compareSync(data.password, user.password);
        if (hash) {
            let token = generar_jwt_1.default.generateToken(user);
            sendUser = {
                id: user.id,
                email: user.email,
                username: user.username,
                full_name: user.full_name,
                type_user: user.type_user,
                status: user.status,
                token: token,
            };
            message = "success";
            jsonRes.data = sendUser;
        }
        else {
            jsonRes.success = false;
            message = "contraseña invalida";
        }
        jsonRes.message = message;
        jsonRes.success = true;
    }
    else {
        jsonRes.message = "usuario no existente";
        jsonRes.success = false;
    }
    jsonRes.code = 200;
    res.json(jsonRes);
});
authController.registrar = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const jsonRes = new Respuesta_model_1.RespuestaModel();
    let data = req.body;
    let user;
    let message = "";
    let sendUser = {};
    user = yield (0, User_model_1.getUserByUsername)({
        username: data.username,
        email: data.email,
    });
    if (user) {
        if (user.username == data.username && user.email != data.email) {
            message = "usuario con username " + data.username + " existente";
        }
        else if (user.email == data.email && user.username != data.username) {
            message = "usuario con email " + data.email + " existente";
        }
        else if (user.username == data.username && user.email == data.email) {
            message =
                "usuario con email " +
                    data.email +
                    " y username " +
                    data.username +
                    " existente";
        }
    }
    if (!user) {
        const hash = yield bcrypt_1.default.hash(data.password, 12);
        data.password = hash;
        let newUser = yield (0, User_model_1.addUser)({
            email: data.email,
            username: data.username,
            full_name: data.full_name,
            password: data.password,
            type_user: "admin",
            status: 1,
        });
        let id_user = newUser.insertId;
        let _user = yield (0, User_model_1.getUser)({
            id: id_user,
        });
        let token = generar_jwt_1.default.generateToken(_user);
        sendUser = {
            id: id_user,
            email: _user.email,
            username: _user.username,
            full_name: _user.full_name,
            type_user: _user.type_user,
            status: _user.status,
            token: token,
        };
        jsonRes.success = true;
        jsonRes.message =
            "El usuario con id: " + id_user + " ha sido registrado exitosamente";
        jsonRes.data = sendUser;
    }
    else {
        jsonRes.success = false;
        jsonRes.message = message;
    }
    jsonRes.code = 200;
    res.json(jsonRes);
});
authController.update = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const jsonRes = new Respuesta_model_1.RespuestaModel();
    let data = req.body;
    let user;
    let message = "";
    let sendUser = {};
    const token = req.header("Authorization");
    let decode = yield generar_jwt_1.default.getAuthorization(token, [
        "admin",
    ]);
    if (decode == false) {
        jsonRes.code = generar_jwt_1.default.code;
        jsonRes.message = generar_jwt_1.default.message;
        jsonRes.success = false;
        jsonRes.data = null;
        return res.json(jsonRes);
    }
    user = yield (0, User_model_1.getUser)({
        id: data.id
    });
    if (user) {
        let uploadUser;
        if (data.password) {
            const hash = yield bcrypt_1.default.hash(data.password, 12);
            data.password = hash;
            uploadUser = {
                id: data.id,
                email: data.email,
                username: data.username,
                full_name: data.full_name,
                password: data.password,
                type_user: "admin",
                status: 1,
            };
        }
        else {
            uploadUser = {
                id: data.id,
                email: data.email,
                username: data.username,
                full_name: data.full_name,
                password: user.password,
                type_user: "admin",
                status: 1,
            };
        }
        let updateQuery = yield (0, User_model_1.updateUser)(uploadUser);
        let _user = yield (0, User_model_1.getUser)({
            id: user.id,
        });
        let token = generar_jwt_1.default.generateToken(_user);
        sendUser = {
            id: _user.id,
            email: _user.email,
            username: _user.username,
            full_name: _user.full_name,
            _user: _user.type_user,
            status: _user.status,
            token: token,
        };
        jsonRes.success = true;
        jsonRes.message =
            "El usuario con id: " + user.id + " ha sido modificado exitosamente";
        jsonRes.data = sendUser;
    }
    else {
        jsonRes.success = false;
        jsonRes.message = "Usuario no encontrado";
    }
    jsonRes.code = 200;
    res.json(jsonRes);
});
exports.default = authController;
