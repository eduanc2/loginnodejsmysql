import { config } from "dotenv";

config();

export const config_db = {
    HOST: "localhost",
    USER: "root",
    PASSWORD: "",
    DB: "test_logindb"
  };
  
export const port = process.env.PORT || 3000;

