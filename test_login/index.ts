import { App } from "./app";
import { connectToDB } from "./database";
async function main() {
  const app = new App();
  app.start();
  await connectToDB();
}
main();
