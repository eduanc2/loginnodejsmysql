import mysql from "mysql2";
import { config_db } from "./config";

export function connectToDB() {
  const connection = mysql.createConnection({
    host: config_db.HOST,
    user: config_db.USER,
    password: config_db.PASSWORD,
    database: config_db.DB,
  });

  return new Promise((resolve, reject) => {
    connection.connect((error) => {
      if (error) {
        reject(error);
      } else {
        console.log("Connected to MySQL");
        resolve(connection);
      }
    });
  });
}
