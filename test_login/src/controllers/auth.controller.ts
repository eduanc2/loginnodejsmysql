import { Request, Response, response } from "express";
import { RespuestaModel } from "../models/Respuesta.model";
import {
  User,
  addUser,
  getUser,
  getUserByEmail,
  getUserByUsername,
  getUsers,
  updateUser,
} from "../models/User.model";
import bcrypt from "bcrypt";

import Jwt from "../helpers/generar-jwt";

const authController: any = {};

authController.login = async (req: Request, res: Response) => {
  const jsonRes = new RespuestaModel();
  let data: any = req.body;
  let sendUser: any = {};
  let user: any;
  let message = "";
  if (data.email) {
    user = await getUserByEmail({
      email: data.email,
    });
    if (user) {
      message = "usuario con email " + data.email + " no encontrado";
    }
  } else if (data.username) {
    user = await getUserByUsername({
      username: data.username,
    });
    if (!user) {
      message = "usuario con username " + data.username + "  no encontrado";
    }
  }

  if (user) {
    const hash = bcrypt.compareSync(data.password, user.password);
    if (hash) {
      let token = Jwt.generateToken(user);
      sendUser = {
        id: user.id,
        email: user.email,
        username: user.username,
        full_name: user.full_name,
        type_user: user.type_user,
        status: user.status,
        token: token,
      };
      message = "success";
      jsonRes.data = sendUser;
    } else {
      jsonRes.success = false;
      message = "contraseña invalida";
    }
    jsonRes.message = message;

    jsonRes.success = true;
  } else {
    jsonRes.message = "usuario no existente"
    jsonRes.success = false;
  }

  jsonRes.code = 200;

  res.json(jsonRes);
};

authController.registrar = async (req: Request, res: Response) => {
  const jsonRes = new RespuestaModel();
  let data: any = req.body;
  let user: any;
  let message = "";
  let sendUser: any = {};

  user = await getUserByUsername({
    username: data.username,
    email: data.email,
  });

  if (user) {
    if (user.username == data.username && user.email != data.email) {
      message = "usuario con username " + data.username + " existente";
    } else if (user.email == data.email && user.username != data.username) {
      message = "usuario con email " + data.email + " existente";
    } else if (user.username == data.username && user.email == data.email) {
      message =
        "usuario con email " +
        data.email +
        " y username " +
        data.username +
        " existente";
    }
  }

  if (!user) {
    const hash = await bcrypt.hash(data.password, 12);
    data.password = hash;
    let newUser: any = await addUser({
      email: data.email,
      username: data.username,
      full_name: data.full_name,
      password: data.password,
      type_user: "admin",
      status: 1,
    });
    let id_user = newUser.insertId;

    let _user: any = await getUser({
      id: id_user,
    });

    let token = Jwt.generateToken(_user);

    sendUser = {
      id: id_user,
      email: _user.email,
      username: _user.username,
      full_name: _user.full_name,
      type_user: _user.type_user,
      status: _user.status,
      token: token,
    };

    jsonRes.success = true;
    jsonRes.message =
      "El usuario con id: " + id_user + " ha sido registrado exitosamente";
    jsonRes.data = sendUser;
  } else {
    jsonRes.success = false;
    jsonRes.message = message;
  }
  jsonRes.code = 200;

  res.json(jsonRes);
};

authController.update = async (req: Request, res: Response) => {
  const jsonRes = new RespuestaModel();
  let data: any = req.body;
  let user: any;
  let message = "";
  let sendUser: any = {};

  const token: any = req.header("Authorization");

  let decode = await Jwt.getAuthorization(token, [
    "admin",
  ]);
  if (decode == false) {
    jsonRes.code = Jwt.code;
    jsonRes.message = Jwt.message;
    jsonRes.success = false;
    jsonRes.data = null;
    return res.json(jsonRes);
  }

  user = await getUser({
    id: data.id
  });


  if (user) {
    let uploadUser:any
    if (data.password) {
      const hash = await bcrypt.hash(data.password, 12);
      data.password = hash;
      uploadUser = {
        id: data.id,
        email: data.email,
        username: data.username,
        full_name: data.full_name,
        password: data.password,
        type_user: "admin",
        status: 1,
      }
    } else {
      uploadUser = {
        id: data.id,
        email: data.email,
        username: data.username,
        full_name: data.full_name,
        password: user.password,
        type_user: "admin",
        status: 1,
      }
    }


    let updateQuery: any = await updateUser(uploadUser);
    let _user: any = await getUser({
      id: user.id,
    });
    
    let token = Jwt.generateToken(_user);

    sendUser = {
      id: _user.id,
      email: _user.email,
      username: _user.username,
      full_name: _user.full_name,
      _user: _user.type_user,
      status: _user.status,
      token: token,
    };

    jsonRes.success = true;
    jsonRes.message =
      "El usuario con id: " + user.id + " ha sido modificado exitosamente";
    jsonRes.data = sendUser;
  } else {
    jsonRes.success = false;
    jsonRes.message = "Usuario no encontrado";
  }
  jsonRes.code = 200;

  res.json(jsonRes);
};

export default authController;
