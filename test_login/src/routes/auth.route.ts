import { Router } from "express";
import authController from "../controllers/auth.controller";


const authRouter = Router();

authRouter.post("/login", authController.login);
authRouter.post("/registrar", authController.registrar);
authRouter.post("/update", authController.update);


export default authRouter;
