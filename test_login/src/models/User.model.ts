import { connectToDB } from "../../database";
export class User {
  public id!: number;
  public email!: string;
  public username!: string;
  public full_name!: string;
  public password!: string;
  public type_user!: string;
  public status!: number;
}

export async function getUsers() {
  try {
    const connection: any = await connectToDB();

    return new Promise((resolve, reject) => {
      connection.query("SELECT * FROM users", (error: any, results: any) => {
        if (error) {
          reject(error);
        } else {
          resolve(results);
        }
      });
    });
  } catch (error) {
    throw error;
  }
}

export async function getUser(data: any) {
  try {
    const connection: any = await connectToDB();

    return new Promise((resolve, reject) => {
      connection.query(
        "SELECT * FROM users WHERE id=" + data.id,
        (error: any, results: any) => {
          if (error) {
            reject(error);
          } else {
            resolve(results[0]);
          }
        }
      );
    });
  } catch (error) {
    throw error;
  }
}

export async function getUserByEmail(data: any) {
  try {
    const connection: any = await connectToDB();

    return new Promise((resolve, reject) => {
      connection.query(
        "SELECT * FROM users WHERE email='" + data.email + "'",
        (error: any, results: any) => {
          if (error) {
            reject(error);
          } else {
            resolve(results[0]);
          }
        }
      );
    });
  } catch (error) {
    throw error;
  }
}
export async function getUserByUsername(data: any) {
  try {
    const connection: any = await connectToDB();

    return new Promise((resolve, reject) => {
      let sql="SELECT * FROM users WHERE username='" + data.username + "' or email='"+data.email+"'";
      connection.query(
        sql
        ,
        (error: any, results: any) => {
          if (error) {
            reject(error);
          } else {
            resolve(results[0]);
          }
        }
      );
    });
  } catch (error) {
    throw error;
  }
}

export async function addUser(data: any) {
  try {
    const connection: any = await connectToDB();

    return new Promise((resolve, reject) => {
      connection.query(
        `INSERT INTO users(email,username,full_name,password,type_user,status) VALUES('${data.email}','${data.username}','${data.full_name}','${data.password}','${data.type_user}',1)`,
        (error: any, results: any) => {
          if (error) {
            reject(error);
          } else {
            resolve(results);
          }
        }
      );
    });
  } catch (error) {
    throw error;
  }
}


export async function updateUser(data: any) {
  try {
    const connection: any = await connectToDB();
    return new Promise((resolve, reject) => {
      connection.query(`UPDATE users SET email = '${data.email}', username = '${data.username}', full_name = '${data.full_name}', password = '${data.password}' WHERE id=${data.id}`,
        (error: any, results: any) => {
          if (error) {
            reject(error);
          } else {
            resolve(results);
          }
        }
      );
    });
  } catch (error) {
    throw error;
  }
}
