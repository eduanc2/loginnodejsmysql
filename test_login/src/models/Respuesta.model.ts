export class RespuestaModel {
    code: number | undefined;
    message: string | undefined;
    success: boolean | undefined;
    data: any;
}